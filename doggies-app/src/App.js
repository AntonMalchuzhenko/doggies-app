import React from 'react';
import CustomSelect from "./CustomSelect";
import logo from './logo.svg';
import './App.css';

function App() {

    const styles = {
      body: {
       width: '300px', paddingTop: '3em', margin: '0 0 0 50em',
      },
      label: {
        color: 'gray',
      },
    };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Welcome to Doggies App.
        </p>
      </header>
        <div style={styles.body}>
            <p style={styles.label}>Choose a doggy</p>
            <CustomSelect style={styles}/>
        </div>
    </div>
  );
}

export default App;
