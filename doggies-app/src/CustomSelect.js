import React from 'react';


const API_PATH = 'http://test.local/api/index.php';

class CustomSelect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.choosedDog = '';
    }

    async handleChange(event) {
        this.setState({value: event.target.value});
        this.choosedDog = event.target.value;
    }

    handleClick(event)
    {
        event.preventDefault();

        fetch(API_PATH, {
                method: 'POST',
                mode: 'no-cors',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({doggy: this.choosedDog})
            }).then((response) => {

            if(response.barking)
            {
                let div = document.getElementById('result');
                div.textContent = response.barking;
            }
        });
    }

    render() {
        return (
            <div>
                <form>
                    <select value={this.state.value} onChange={this.handleChange}>
                        <option value="0">Please select...</option>
                        <option value="shiba_inu_dog">Shiba-inu</option>
                        <option value="pug_dog">Pug-dog</option>
                        <option value="dachshund_dog">Dachshund-dog</option>
                        <option value="plush_labrador_dog_toy">Plush labrador</option>
                        <option value="rubber_dachshund_dog_toy">Rubber dachshund</option>
                    </select>
                    <input type="submit" onClick={this.handleClick} value="Sound"/>
                </form>
                <div id="result"></div>
            </div>
        );
    }
}

export default CustomSelect;