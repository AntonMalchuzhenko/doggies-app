<?php
require_once  '../classes/Dog.php';

/*** Configuration this headers in Nginx server-block ***/
//header("Access-Control-Allow-Origin: *");
//header("Access-Control-Allow-Methods: POST");
//header("Access-Control-Allow-Headers: access");
//header("Access-Control-Allow-Credentials: true");
/********************************************************/
header("Content-Type: application/json; charset=UTF-8");


$data = json_decode(file_get_contents('php://input'), true);

if(isset($data['doggy']) && $data['doggy'] != '')
{
  $dog = new Dog($data['doggy']);

  http_response_code(200);
  file_put_contents('php://output', json_encode(['barking' => $dog->doggyBark()]));
  die;
}
else
  http_response_code(404);
die;