<?php


class Dog
{
  private $type;

  public function __construct($doggy_type)
  {
    $this->setDoggyType($doggy_type);
  }


  public function doggyBark()
  {
    $sound = false;
    $doggy_type =$this->getDoggyType();

    if($doggy_type) {

      switch ($doggy_type) {

        case 'shiba_inu_dog':
          $sound = 'Shiba-inu barking: Woof woof wooooof!';
          break;

        case 'pug_dog':
          $sound = 'Pug dog barking: Woof woof rrrrrr!';
          break;

        case 'dachshund_dog':
          $sound = 'Pug dog barking: Woof woof, rrrrrr, woof woof !';
          break;

        case 'plush_labrador_dog_toy':
          $sound = 'A plush labrador cannot bark';
          break;

        case 'rubber_dachshund_dog_toy':
          $sound = 'rubber dachshund can only squeak';
          break;
      }

    } else {
      http_response_code(404);
      echo 'Doggy not found. Soryan :)';
    }
    return $sound;
  }

  public function getDoggyType()
  {
    return $this->type ?? $this->type;
  }

  private function setDoggyType($type)
  {
    $this->type = $type;
  }

}